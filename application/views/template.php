<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8"/>
    <title>Sistem Arsip PGE || <?php echo $title; ?></title>
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/template/css/layout.css" type="text/css" media="screen" />
    <!--link rel="stylesheet" href="<?php //echo base_url(); ?>assets/template/js/themes/base/jquery.ui.all.css"-->
    <!--[if lt IE 9]>
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/template/css/ie.css" type="text/css" media="screen" />
    <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->
    <?php foreach($js_files as $file): ?>
        <script src="<?php echo $file; ?>"></script>
    <?php endforeach; ?>


    <!--script src="js/jquery-1.5.2.min.js" type="text/javascript"></script-->

    <?php foreach($css_files as $file): ?>
        <link type="text/css" rel="stylesheet" href="<?php echo $file; ?>" />
    <?php endforeach; ?>

    <!--JQUERY UI-->
    <!--script src="<?php //echo base_url(); ?>assets/template/js/jquery-1.8.3.js"></script>
    <script src="<?php //echo base_url(); ?>assets/template/js/ui/jquery.ui.core.js"></script>
    <script src="<?php //echo base_url(); ?>assets/template/js/ui/jquery.ui.widget.js"></script>
    <script src="<?php //echo base_url(); ?>assets/template/js/ui/jquery.ui.datepicker.js"></script-->
    <!--JQUERY UI-->



    <script src="<?php echo base_url(); ?>assets/template/js/hideshow.js" type="text/javascript"></script>
    <script src="<?php echo base_url(); ?>assets/template/js/jquery.tablesorter.min.js" type="text/javascript"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>assets/template/js/jquery.equalHeight.js"></script>


    <script type="text/javascript">
        $(document).ready(function()
            {
                $(".tablesorter").tablesorter();
            }
        );

        $(document).ready(function() {

            //When page loads...
            $(".tab_content").hide(); //Hide all content
            $("ul.tabs li:first").addClass("active").show(); //Activate first tab
            $(".tab_content:first").show(); //Show first tab content

            //On Click Event
            $("ul.tabs li").click(function() {

                $("ul.tabs li").removeClass("active"); //Remove any "active" class
                $(this).addClass("active"); //Add "active" class to selected tab
                $(".tab_content").hide(); //Hide all tab content

                var activeTab = $(this).find("a").attr("href"); //Find the href attribute value to identify the active tab + content
                $(activeTab).fadeIn(); //Fade in the active ID content
                return false;
            });

        });
    </script>
    <script type="text/javascript">
        $(function(){
            $('.column').equalHeight();
        });
    </script>
    <!-- TinyMCE -->
    <!--script type="text/javascript" src="<?php //echo base_url(); ?>assets/template/mce/jscripts/tiny_mce/tiny_mce.js"></script-->
    <script type="text/javascript">
//        tinyMCE.init({
//            // General options
//            mode : "exact",
//            elements:"elm1",
//            theme : "advanced",
//            skin : "o2k7",
//            plugins : "autolink,lists,pagebreak,style,layer,table,save,advhr,advimage,advlink,emotions,iespell,insertdatetime,preview,media,searchreplace,print,contextmenu,paste,directionality,fullscreen,noneditable,visualchars,nonbreaking,xhtmlxtras,template,inlinepopups,autosave",
//
//            // Theme options
//            theme_advanced_buttons1 : "save,newdocument,|,bold,italic,underline,strikethrough,|,justifyleft,justifycenter,justifyright,justifyfull,|,styleselect,formatselect,fontselect,fontsizeselect",
//            theme_advanced_buttons2 : "cut,copy,paste,pastetext,pasteword,|,search,replace,|,bullist,numlist,|,outdent,indent,blockquote,|,undo,redo,|,link,unlink,anchor,image,cleanup,help,code,|,insertdate,inserttime,preview,|,forecolor,backcolor",
//            theme_advanced_buttons3 : "tablecontrols,|,hr,removeformat,visualaid,|,sub,sup,|,charmap,emotions,iespell,media,advhr,|,print,|,ltr,rtl,|,fullscreen",
//            theme_advanced_buttons4 : "insertlayer,moveforward,movebackward,absolute,|,styleprops,|,cite,abbr,acronym,del,ins,attribs,|,visualchars,nonbreaking,template,pagebreak,restoredraft",
//            theme_advanced_toolbar_location : "top",
//            theme_advanced_toolbar_align : "left",
//            theme_advanced_statusbar_location : "bottom",
//            theme_advanced_resizing : true,
//
//            // Example word content CSS (should be your site CSS) this one removes paragraph margins
//            content_css : "mce/css/word.css",
//
//            // Drop lists for link/image/media/template dialogs
//            template_external_list_url : "mce/lists/template_list.js",
//            external_link_list_url : "mce/lists/link_list.js",
//            external_image_list_url : "mce/lists/image_list.js",
//            media_external_list_url : "mce/lists/media_list.js",
//
//            // Replace values for the template plugin
//            template_replace_values : {
//                username : "Some User",
//                staffid : "991234"
//            }
//        });
    </script>
    <!-- /TinyMCE -->
    <script type="text/javascript">
        function teruskan_disposisi(){
            if(document.ceklis.cek.checked==true){
                $('#terusan').fadeIn();
                $('#terusan').html(data);
            }else if(document.ceklis.cek.checked==false){
                $('#terusan').fadeOut();
            }
        }
        function ganti_halaman(value){
            window.location=value;
        }
        function search_data(table,nilai){
            $.ajax({
                url:'pencarian.php?tbl='+table+'&val='+nilai+'',
                beforeSend:function(){
                    document.getElementById('tab1').innerHTML="Loading....";
                },
                success:function(data){
                    $('#tab1').fadeIn();
                    $('#tab1').html(data);
                }
            });
        }
    </script>
</head>


<body>

<header id="header">
    <hgroup>
        <h1 class="site_title"><a href="index.php">Website Admin</a></h1>
        <h2 class="section_title">Sistem Arsip Pertamina Geothermal Energy Lumut Balai</h2>
        <!--<div class="btn_view_site"><a href="index.php">View Site</a></div>-->
    </hgroup>
</header> <!-- end of header bar -->

<section id="secondary_bar">
    <div class="user">
        <p>User Name</p>
        <!-- <a class="logout_user" href="#" title="Logout">Logout</a> -->
    </div>
    <div class="breadcrumbs_container">
        <!--<article class="breadcrumbs"><a href="index.html">Website Admin</a> <div class="breadcrumb_divider"></div> <a class="current">Dashboard</a></article>-->
    </div>
</section><!-- end of secondary bar -->

<aside id="sidebar" class="column">
    <form class="quick_search">
        <table border="0" style="margin-left:50px;">
            <tr><td width="50px"><b>User</b></td><td>:</td><td> username</td></tr>
            <tr><td><b>Fungsi</b></td><td>:</td><td> level</td></tr>
        </table>
    </form>
    <hr/>
    <?php if( $sql_menu->num_rows() ): ?>
        <?php foreach( $sql_menu->result() as $field => $vmenu ): ?>
            <h3><?php echo $vmenu->nama_fungsi; ?></h3>
            <ul class="toggle">
                <li class="icn_new_article"><a href="<?php echo base_url(); ?>manage/detail_arsip/<?php echo $vmenu->id_jenis_arsip; ?>"><?php echo $vmenu->jenis_surat; ?></a></li>
            </ul>
        <?php endforeach; ?>
    <?php endif; ?>

        <h3>Pengelolaan Surat Keluar</h3>
        <ul class="toggle">
            <li class="icn_new_article"><a href="<?php echo base_url(); ?>manage/surat_keluar/">Surat Keluar</a></li>
            <li class="icn_new_article"><a href="<?php echo base_url(); ?>manage/surat_keluar_action/add">Pendataan Surat Keluar</a></li>

            <li class="icn_profile">&nbsp;</li>
        </ul>
        <?php if( $this->session->userdata( 'LEVEL' ) == 'admin' ): ?>
        <h3>Pengarsipan Surat Masuk</h3>
        <ul class="toggle">
            <li class="icn_new_article"><a href="<?php echo base_url(); ?>manage/penerimaan_surat/add">Penerimaan Surat</a></li>

            <li class="icn_profile">&nbsp;</li>
        </ul>
        <?php else: ?>
        <h3>Pengarsipan Surat Masuk</h3>
        <ul class="toggle">
            <li class="icn_new_article"><a href="<?php echo base_url(); ?>manage/penerimaan_surat/<?php echo $this->session->userdata( 'FUNGSI' ); ?>">Penerimaan Surat</a></li>

            <li class="icn_profile">&nbsp;</li>
        </ul>

        <?php endif; ?>

        <?php if( $this->session->userdata( 'LEVEL' ) == 'admin' ): ?>
        <h3>Admin</h3>
        <ul class="toggle">
            <li class="icn_categories"><a href="<?php echo base_url(); ?>manage/fungsi">Fungi</a></li>
            <li class="icn_categories"><a href="<?php echo base_url(); ?>manage/sub_fungsi_arsip">Sub Fungsi</a></li>
            <li class="icn_settings"><a href="<?php echo base_url(); ?>manage/akun">Pengaturan Akun</a></li>

        <?php elseif( $this->session->userdata( 'LEVEL' ) ): ?>
        <h3>Admin</h3>
        <ul class="toggle">
            <li class="icn_categories"><a href="<?php echo base_url(); ?>manage/profil/edit/<?php echo $this->session->userdata(  'USERAME') ?>">Profil</a></li>
        <?php endif; ?>
            <li class="icn_jump_back"><a href="<?php echo base_url(); ?>logout">Logout</a></li>
            <li> </li>
            <li> </li>
            <li class="icn_profile">&nbsp;</li>
            <li class="icn_profile">&nbsp;</li>
            <li class="icn_profile">&nbsp;</li>
            <li class="icn_profile">&nbsp;</li>
            <li class="icn_profile">&nbsp;</li>
            <li class="icn_profile">&nbsp;</li>
            <li class="icn_profile">&nbsp;</li>
        </ul>
        <footer>
            <hr />
            <p><strong>Copyright &copy; PGE Lumut Balai</strong></p>
            <p>Theme by <a href="http://www.medialoot.com">MediaLoot</a></p>
        </footer>
</aside><!-- end of sidebar -->

<section id="main" class="column">
    <?php echo $print; ?>
</section>

</body>

</html>